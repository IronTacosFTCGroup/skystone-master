package org.firstinspires.ftc.teamcode.RoverRukus;
// test 10/23

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
 */
@TeleOp
@Disabled

public class driveTrainTest_V_0_1 extends LinearOpMode {

    // Properties
    /**
     * Declares our actuators
     */
    private DcMotor leftDriveMotor; // Left drive motor
    private DcMotor rightDriveMotor; // Right drive motor


    @Override
    public void runOpMode() {
        // Properties
        // Gets hardware info and names each actuator
        leftDriveMotor = hardwareMap.get(DcMotor.class, "leftDriveMotor"); // Left drive motor
        rightDriveMotor = hardwareMap.get(DcMotor.class, "rightDriveMotor"); // Right drive motor


        //Names all variables, sets values
        double leftWheelPower = 0; // Used for controling power on the left drive motor.
        double rightWheelPower = 0; // Used for controling power on the right drive motor


        double WHEEL_POWER_MULT = 0.5; // the power to the wheels

        //Configures motors to use encoders and brake when no power is applied
        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);



        leftDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        // Relays status to driver station
        telemetry.addData("Status", "Initialized");
        telemetry.update();


        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // Get gamepad variables
            leftWheelPower = this.gamepad1.left_stick_y * WHEEL_POWER_MULT;
            rightWheelPower = this.gamepad1.right_stick_y * WHEEL_POWER_MULT;

            // Turbo Boost
            if (this.gamepad1.right_trigger > 0.15) {
                leftWheelPower *= 2;
                rightWheelPower *= 2;
            }

            // Driving commands
            leftDriveMotor.setPower(leftWheelPower);
            rightDriveMotor.setPower(rightWheelPower);

            telemetry.update();

            idle();
        } // while op mode active
    } // runOpMode()
}
