package org.firstinspires.ftc.teamcode.SkyStone;



import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

import edu.spa.ftclib.internal.drivetrain.MecanumDrivetrain;

/**
 * Created by Michaela on 1/3/2018.
 */

@Disabled
//@TeleOp(name = "Mecanum Robot Tele-op", group = "sample")
@TeleOp
public class MecanumTeleOpFunctionColorSensor extends OpMode {



    public DcMotor frontLeft;           //front left motor
    public DcMotor frontRight;          //front right motor
    public DcMotor backLeft;            //back left motor
    public DcMotor backRight;           //back right motor

    public DcMotor armLiftMotor;        //motor that lifts arm
    public DcMotor armExtendMotor;      //motor that extends arm
    public Servo clampOTron;            //hand that grabs the blocks
    public Servo backChompinator;          //base grabbers
    public Servo frontChompinator;
    public CRServo grabberPivoter;      // Spins the clampotron
    public Servo blockClamp;

    public ColorSensor colorSensor;

    public double ARM_LOCK_POWER = 0.00071; // Power to hold the arm in one place
    public double MAX_ARM_POWER = 0.4; // Arm power limit
    public double MIN_ARM_POWER = 0.1; // Minimum power
    public double STEEPNESS = 0.008; // How fast the power output changes
    public double MIDPOINT = 1000; // Sigmoid function midpoint
    public double LOWER_SPEED = 0.25;

    public double exponent = 0.0;

    public boolean armLockToggle = false;
    public boolean xPressed = false;
    public boolean xNotPressed = true;
    public boolean yPressed = false;
    public boolean yNotPressed = true;
    public boolean bumpPressed = false;
    public boolean bumpNotPressed = true;
    public boolean raiseAndLockArmToggle = false;
    public boolean raising = false;
    public boolean autoLevel = false;


    public double armExtendMult = 0.2;
    public double pivoterMult = 0.5;
    public double precisionMult = 0.2;
    public double rotationMult = 0.5;
    public double selfLevelMult = 0.8;
    public double positionThreshold1 = 1000;
    public double positionThreshold2 = 2000;
    public double positionThreshold3 = 3000;
    public double armMotorPower = 0.0;
    public double armExtendPower = 0.0;
    public int armPos = 0;
    public double pivoterPower = 0.0;

    public MecanumDrivetrain drivetrain;

    /**
     * User defined init method
     * <p>
     * This method will be called once when the INIT button is pressed.
     */
    @Override
    public void init() {
        frontLeft = hardwareMap.get(DcMotor.class, "driveFrontLeft");
        frontRight = hardwareMap.get(DcMotor.class, "driveFrontRight");
        backLeft = hardwareMap.get(DcMotor.class, "driveBackLeft");
        backRight = hardwareMap.get(DcMotor.class, "driveBackRight");

        armLiftMotor = hardwareMap.get(DcMotor.class, "armLiftMotor");
        armExtendMotor = hardwareMap.get(DcMotor.class, "armExtendMotor");
        clampOTron = hardwareMap.get(Servo.class, "clampOTron");
        backChompinator = hardwareMap.get(Servo.class, "chompinator1");
        frontChompinator = hardwareMap.get(Servo.class, "chompinator2");
        grabberPivoter = hardwareMap.get(CRServo.class, "grabberPivoter");
        blockClamp = hardwareMap.get(Servo.class, "blockClamp");

        colorSensor = hardwareMap.get(ColorSensor.class, "colorSensor");

        drivetrain = new MecanumDrivetrain(new DcMotor[]{frontLeft, frontRight, backLeft, backRight});

        armLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        armLiftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armExtendMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        armLiftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        armLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);


    }

    // Misc functions

    public double ArmRaiseFunction (float leftStickY, int armRaiseMotorPos, double midpoint) {
        exponent = (STEEPNESS*(armRaiseMotorPos - midpoint));
        // Passes in exponent to equation

        return leftStickY * ((MAX_ARM_POWER-MIN_ARM_POWER) / (1+ Math.exp(exponent)) + MIN_ARM_POWER);
        // Returns arm motor power



    }

    public double getArmRaiserPower(float leftStickY, int armRaiseMotorPos)
    {


        if (leftStickY > 0)
        {
            // Front to back

            return ArmRaiseFunction(leftStickY, armRaiseMotorPos, MIDPOINT);

        } else if (leftStickY < 0)
        {
            // Back to front

            return ArmRaiseFunction(leftStickY, -armRaiseMotorPos, -MIDPOINT);
        } else
        {
            // No stick input
            return 0.0;
        }
    }


    public void raiseAndLockArmSwitch() {
        // Arm raise and lock function toggle
        if (!gamepad2.y) {
            yNotPressed = true;
        }

        if (yNotPressed == true && this.gamepad2.y) {
            yPressed = true;
            yNotPressed = false;
        }

        if (yPressed == true) {
            raiseAndLockArmToggle = !raiseAndLockArmToggle;
            yPressed = false;
        }
    }



    public void raiseAndLockArm() {

        if (armPos > 400) {
            armLiftMotor.setPower(-0.4);
            raising = true;
        }

        if (armPos < 200) {
            armLiftMotor.setPower(0.4);
            raising = true;

        }

        if (armPos > 200 && armPos < 400) {
            armMotorPower =  ARM_LOCK_POWER;
            raiseAndLockArmToggle = false;
            armLockToggle = true;
            raising = false;
        }
    }



    // Teleop actuator functions:


    public void chompControls() {
        // Chompinator controls (Foundation Grippers)
        if (this.gamepad1.x)
        {
            // Open
            backChompinator.setPosition(0.22);
            frontChompinator.setPosition(0.53);
        }

        if (this.gamepad1.y)
        {
            // Close
            backChompinator.setPosition(0.53);
            frontChompinator.setPosition(0.22);
        }

        if (this.gamepad1.a) {
            blockClamp.setPosition(0.85);
        }

        if (this.gamepad1.b) {
            blockClamp.setPosition(0.5);
        }

    }




    public void clampControls() {
        //Clamp-O-Tron controls (Block Clamp)
        if (this.gamepad2.a) {
            // Close
            clampOTron.setPosition(0.42);
        }

        if (this.gamepad2.b) {
            // open
            clampOTron.setPosition(0.65);
        }
    }




    public void grabberPointerControls() {


        // Arm lock / unlock
        if (!gamepad2.left_bumper) {
            bumpNotPressed = true;
        }

        if (bumpNotPressed == true && this.gamepad2.left_bumper) {
            bumpPressed = true;
            bumpNotPressed = false;
        }

        if (bumpPressed == true) {
            autoLevel = !autoLevel;
            bumpPressed = false;
        }


        // Sets power to grabber pivoting CR servo
        if (gamepad2.left_trigger > 0.1 && gamepad2.right_trigger > 0.1) {
            pivoterPower = 0.0;
        }
        else if (gamepad2.left_trigger > 0.1) {
            pivoterPower = gamepad2.left_trigger * pivoterMult;
            grabberPivoter.setDirection(DcMotorSimple.Direction.FORWARD);
        }
        else if (gamepad2.right_trigger > 0.1) {
            pivoterPower = gamepad2.right_trigger * pivoterMult;
            grabberPivoter.setDirection(DcMotorSimple.Direction.REVERSE);
        }
        else {
            pivoterPower = 0.0;
        }

        // Update power
        grabberPivoter.setPower(pivoterPower);
    }

    public void armExtendControls() {
        armExtendPower = gamepad2.right_stick_y * armExtendMult;
        armExtendMotor.setPower(armExtendPower);
    }

    public void grabberPointerLevel() {


        // Arm lock / unlock
        if (!gamepad2.left_bumper) {
            bumpNotPressed = true;
        }

        if (bumpNotPressed == true && this.gamepad2.left_bumper) {
            bumpPressed = true;
            bumpNotPressed = false;
        }

        if (bumpPressed == true) {
            autoLevel = !autoLevel;
            bumpPressed = false;
        }


        if (armPos < 900 && armMotorPower > -0.07) {
            grabberPivoter.setDirection(DcMotorSimple.Direction.FORWARD);
            grabberPivoter.setPower(armMotorPower * selfLevelMult);
        }








    }

    public void armLiftControls() {

        // Update variables
        armPos = armLiftMotor.getCurrentPosition();

        // If arm is unlocked, set power
        if (armLockToggle == false && raising == false) {
            armMotorPower = getArmRaiserPower(gamepad2.left_stick_y, armPos);
        }

        // Locked arm
        if (armLockToggle == true) {
            if (armPos > 1200) {
                armMotorPower = -ARM_LOCK_POWER;
            }

            if (armPos < 800) {
                armMotorPower = ARM_LOCK_POWER;
            }
        }

        // Arm lock / unlock
        if (!gamepad2.x) {
            xNotPressed = true;
        }

        if (xNotPressed == true && this.gamepad2.x) {
            xPressed = true;
            xNotPressed = false;
        }

        if (xPressed == true) {
            armLockToggle = !armLockToggle;
            xPressed = false;
        }

        // Update arm power
        armLiftMotor.setPower(armMotorPower);

    }


    public void drivetrainControls() {
        // Update drivetrain variables
        double course = Math.atan2(gamepad1.right_stick_y, gamepad1.right_stick_x) - Math.PI/2;
        double velocity = Math.hypot(gamepad1.right_stick_x, gamepad1.right_stick_y);
        double rotation = gamepad1.left_stick_x * rotationMult;

         if (gamepad1.right_trigger > 0) {
            velocity = velocity * precisionMult;
            rotation = rotation * precisionMult;
        }

        // Update drivetrain commands
        drivetrain.setCourse(course);
        drivetrain.setVelocity(velocity);
        drivetrain.setRotation(rotation);



    }

    public void runTelemetry() {
        // Telemetry


        telemetry.addData("blue", colorSensor.blue());
        telemetry.addData("red", colorSensor.red());
        telemetry.addData("green", colorSensor.green());
        telemetry.addData("armPos", armPos);
        telemetry.addData("Arm Power", armMotorPower);
        telemetry.addData("armMotorPower", armMotorPower);
        telemetry.addData("lefty", -gamepad2.left_stick_y);
        telemetry.addData("Arm Lcok", armLockToggle);
        telemetry.update();
    }

    public void runTeleOpCommands() {

        chompControls();

        clampControls();

        if (autoLevel == true) {
            grabberPointerLevel();
        }

        if (autoLevel == false) {
            grabberPointerControls();
        }

        drivetrainControls();

        runTelemetry();

        raiseAndLockArmSwitch();

        armExtendControls();

        armLiftControls();


        if (raiseAndLockArmToggle == true) {
            raiseAndLockArm();
        }

    }



    /**
     * User defined loop method
     * <p>
     * This method will be called repeatedly in a loop while this op mode is running
     */
    @Override
    public void loop() {

        runTeleOpCommands();

    }
}

