package org.firstinspires.ftc.teamcode.RoverRukus;
// test 10/23

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;


/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
 */
@Autonomous
@Disabled

public class AutonomousV_0_4_Depot extends LinearOpMode {

    //Actuators
    private DcMotor leftDriveMotor; // Left drive motor
    private DcMotor rightDriveMotor; // Right drive motor
    private DcMotor sweeperMotor; // Pepe motor
    private DcMotor robotLiftMotor; // lifting motor
    private DcMotor armRaiseMotor; //  arm raising motor

    private Servo hookServo; // hook servo


    @Override
    public void runOpMode() {
        // Properties
        // Gets hardware info and names each actuator
        leftDriveMotor = hardwareMap.get(DcMotor.class, "leftDriveMotor"); // Left drive motor
        rightDriveMotor = hardwareMap.get(DcMotor.class, "rightDriveMotor"); // Right drive motor
        sweeperMotor = hardwareMap.get(DcMotor.class, "sweeperMotor"); // Pepe motor
        robotLiftMotor = hardwareMap.get(DcMotor.class, "robotLiftMotor"); // robot lifting motor
        armRaiseMotor = hardwareMap.get(DcMotor.class, "armRaiseMotor"); // robot lifting motor

        hookServo = hardwareMap.get(Servo.class, "hookServo"); // hook servo


        // Creates Phases
        final int LOWER_ROBOT  = 1;
        final int DRIVE_FROM_LANDER = 2;
        final int VOMIT_PEPE = 3;
        final int IDLE = 8;

        // Sets phase
        int currentPhase = LOWER_ROBOT;

        //Names all variables, sets values
        double linActLowerDistance = -26550; // used to change the distance we lower the robot
        double straightDriveDistance = -2300; // driving away from the lander


        double WHEEL_POWER_MLUT = 0.3; // the power to the wheels
        double LOWER_ROBOT_POWER_MLUT = 4.0; // Controls how fast linear actuator extends
        double SWEEPER_POWER_MULT = 0.5; // the power of the sweeper

        int leftDrivePos = 0; // Used to measure the left drive motor's position
        int robotLiftPos = 0; // Used to measure the linear actuator motor's position;

        // Configures motors to use encoders and brake when no power is applied
        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        leftDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robotLiftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armRaiseMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // Relays status to driver station.
        telemetry.addData("Status", "Initialized");
        telemetry.update();


        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Run until the end of the match (driver presses STOP)/
        while (opModeIsActive()) {
            // Update Variables
            leftDrivePos = leftDriveMotor.getCurrentPosition();
            robotLiftPos = robotLiftMotor.getCurrentPosition();

            telemetry.addData("WheelP", leftDrivePos);
            telemetry.addData("Lin Act Pos", robotLiftPos);

            switch (currentPhase)
            {

                case LOWER_ROBOT:

                    telemetry.addData("Status", "Lowering");


                    if (robotLiftPos  > linActLowerDistance) {
                        robotLiftMotor.setPower(-1 * LOWER_ROBOT_POWER_MLUT);
                        telemetry.addData("Distance Lowered", robotLiftPos);
                    }
                    else
                    {
                        robotLiftMotor.setPower(0.0);
                        hookServo.setPosition(0.85);
                        sleep(1000);
                        currentPhase = DRIVE_FROM_LANDER;
                    }

                break;

                case DRIVE_FROM_LANDER:

                    telemetry.addData("Status", "Driving Depot");

                    robotLiftMotor.setPower(1);

                    if (leftDrivePos > straightDriveDistance) {
                        leftDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                        rightDriveMotor.setPower(1 * WHEEL_POWER_MLUT);



                    }
                    else {
                        leftDriveMotor.setPower(0.0);
                        rightDriveMotor.setPower(0.0);
                        currentPhase = VOMIT_PEPE;
                    }
                    break;



                case VOMIT_PEPE:

                    telemetry.addData("Status", "Depositing");

                    armRaiseMotor.setPower(-0.3);
                    sleep(1000);

                    sweeperMotor.setPower(-1 * SWEEPER_POWER_MULT);
                    sleep(2000);
                    sweeperMotor.setPower(0.0);
                    armRaiseMotor.setPower(0.0);
                    robotLiftMotor.setPower(0.0);

                    currentPhase = IDLE;

                break;

                case IDLE:

                    // In case we want to do anything while stopped

                default: // something is wrong, stop motors


                        robotLiftMotor.setPower(0);
                        leftDriveMotor.setPower(0);
                        rightDriveMotor.setPower(0);
                        sweeperMotor.setPower(0);
                        telemetry.addData("F", "in the chat");


                break;

            }// End switching phases
            telemetry.update();
        } // while op mode active
    } // runOpMode()
}
