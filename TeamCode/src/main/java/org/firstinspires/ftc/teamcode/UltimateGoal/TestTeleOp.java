package org.firstinspires.ftc.teamcode.UltimateGoal;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

import edu.spa.ftclib.internal.drivetrain.MecanumDrivetrain;

/**

 Controls Index:

 Gamepad 1 (Driver):
 Buttons:
 A -
 B -
 X -
 Y -
 Joysticks:
 Left -
 Right -
 Triggers:
 Left -
 Right -
 Bumpers:
 Left -
 Right -
 D-pad:
 Up -
 Down -
 Left -
 Right -

 Gamepad 2 (Operator/Gunner):
 Buttons:
 A -
 B -
 X -
 Y -
 Joysticks:
 Left -
 Right -
 Triggers:
 Left -
 Right -
 Bumpers:
 Left -
 Right -
 D-pad:
 Up -
 Down -
 Left -
 Right -

 */

//@Disabled
//@TeleOp(name = "Mecanum Robot Tele-op", group = "sample")
@TeleOp
public class TestTeleOp extends OpMode {


    // Actuator definitions
    public DcMotor frontLeft;           // Front left drive motor
    public DcMotor frontRight;          // Front right drive motor
    public DcMotor backLeft;            // Rear left drive motor
    public DcMotor backRight;           // Rear right drive motor
    public DcMotor launcher;            // Launch wheel motor
    public DcMotor carousel;            // Carousel wheel motor
    public DcMotor intake;              // Intake wheel motor
    public DcMotor armLift;             // Arm-lifting motor

    public Servo wobbleClamp;           // This servo clamps onto the wobble goal


    // Variable and multipliers
    int carouselPosition = 0;
    int targetCarouselPosition = 0;
    double armPower = 0.0;
    double armMultiplier = -0.35;

    /**
     * User defined init method
     * <p>
     * This method will be called once when the INIT button is pressed.
     */
    @Override
    public void init() {


        // Actuator profiles
        frontLeft = hardwareMap.get(DcMotor.class, "frontLeft");
        frontRight = hardwareMap.get(DcMotor.class, "frontRight");
        backLeft = hardwareMap.get(DcMotor.class, "backLeft");
        backRight = hardwareMap.get(DcMotor.class, "backRight");
        launcher = hardwareMap.get(DcMotor.class, "launcher");
        carousel = hardwareMap.get(DcMotor.class, "carousel");
        intake = hardwareMap.get(DcMotor.class, "intake");
        armLift = hardwareMap.get(DcMotor.class, "armLift");

        wobbleClamp = hardwareMap.get(Servo.class, "wobbleClamp");

        // Actuator configurations
        armLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        carousel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        armLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        carousel.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        armLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        carousel.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        armLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        carousel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);


    }

    /**
     * This teleOp is separated into a number of different functions
     * Each function is responsible for controlling a specific system on the robot
     * All the functions are continuously called in the while loop to operate the robot
      */

   public void wobbleArmControls() {
       // This function controls the wobble goal arm
       // It actuates the clamp servo and the arm motor

       if (gamepad2.a) {
           // Closed
           wobbleClamp.setPosition(0.7);
       }

       if (gamepad2.b) {
           // Open
           wobbleClamp.setPosition(0.3);
       }

       armPower = armMultiplier * gamepad2.left_stick_y;
       armLift.setPower(armPower);

/**
 * Include:
 * Wobble arm motor control
 * Locking function
 * Clamp servo functions
 */

   }

   public void intakeControls() {
       // This function controls the wheel intake
       // It changes the speed of the intake motor

       if (gamepad2.dpad_up) {
           intake.setPower(-1.0);
       }

       if (gamepad2.dpad_down) {
           intake.setPower(0.5);
       }

       if (gamepad2.dpad_right) {
           intake.setPower(0.0);
       }

/**
 * Include:
 * Spinner motor
 * Carousel (if using pusher)
 * Speed controls and reverse functionality
 */



   }

    public void launcherControls() {
        // This function controls the trigger mechanism and launch wheel

        carouselPosition = carousel.getCurrentPosition();

        if (gamepad2.dpad_left) {
            carousel.setTargetPosition(carouselPosition - 1250);
            carousel.setPower(-0.3);
            targetCarouselPosition = carouselPosition - 1250;
        }

        else if (carouselPosition < targetCarouselPosition) {
            carousel.setPower(0.0);
        }

        if (gamepad2.x) {
            launcher.setPower(-1.0);
        }

        if (gamepad2.y) {
            launcher.setPower(0.0);
        }
/**
 * Include:
 * Launch Wheel
 * Trigger mech (carousel or pusher)
 */



    }


    public void drivetrainControls() {
        // This function controls the robot's mecanum drivetrain
/**
 * Include:
 * Mecanum drivetrain controls
 * Slow mode
 * Return to firing position code
 */

    }

    public void runTelemetry() {
        // This function returns telemetry to the robot controller
/**
 * Include:
 * Telemetry
 * Carousel status
 */

    }

    public void runTeleOpCommands() {
        // This function calls all of the robot's systems with one function
/**
 * Include:
 * All systems
 */

        intakeControls();

        launcherControls();

        wobbleArmControls();

        runTelemetry();


    }



    /**
     * User defined loop method
     * <p>
     * This method will be called repeatedly in a loop while this op mode is running
     */
    @Override
    public void loop() {
        // This loop calls all the TeleOp control systems while the robot runs
        runTeleOpCommands();

    }
}

