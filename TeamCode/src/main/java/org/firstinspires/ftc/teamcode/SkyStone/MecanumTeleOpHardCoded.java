package org.firstinspires.ftc.teamcode.SkyStone;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.CRServo;

import java.util.concurrent.locks.Lock;

import edu.spa.ftclib.internal.drivetrain.MecanumDrivetrain;

/**
 * Created by Michaela on 1/3/2018.
 */

@Disabled
//@TeleOp(name = "Mecanum Robot Tele-op", group = "sample")
@TeleOp
public class MecanumTeleOpHardCoded extends OpMode {



    public DcMotor frontLeft;           //front left motor
    public DcMotor frontRight;          //front right motor
    public DcMotor backLeft;            //back left motor
    public DcMotor backRight;           //back right motor

    public DcMotor armLiftMotor;        //motor that lifts arm
    public DcMotor armExtendMotor;      //motor that extends arm
    public Servo clampOTron;            //hand that grabs the blocks
    public Servo backChompinator;          //base grabbers
    public Servo frontChompinator;
    public CRServo grabberPivoter;      //spins the clampotron

    public double maxArmPowerMult = 0.4;    //when the arm is at the lowest setting, and at the highest power
    public double midArmPowerMult = 0.2;    //when the arm is at the middle setting, and at the middle power
    public double minArmPowerMult = 0.1;    //when the arm is at the highest setting, and at the lowest power

    public double armLockPower = 0.00071;

    public boolean armLockToggle = false;
    public boolean xPressed = false;
    public boolean xNotPressed = true;

    public double armExtendMult = 0.2;
    public double pivoterMult = 0.5;
    public double positionThreshold1 = 1000;
    public double positionThreshold2 = 2000;
    public double positionThreshold3 = 3000;
    public double armMotorPower = 0.0;
    public double armExtendPower = 0.0;
    public int armPos = 0;
    public double pivoterPower = 0.0;

    public MecanumDrivetrain drivetrain;

    /**
     * User defined init method
     * <p>
     * This method will be called once when the INIT button is pressed.
     */
    @Override
    public void init() {
        frontLeft = hardwareMap.get(DcMotor.class, "driveFrontLeft");
        frontRight = hardwareMap.get(DcMotor.class, "driveFrontRight");
        backLeft = hardwareMap.get(DcMotor.class, "driveBackLeft");
        backRight = hardwareMap.get(DcMotor.class, "driveBackRight");

        armLiftMotor = hardwareMap.get(DcMotor.class, "armLiftMotor");
        armExtendMotor = hardwareMap.get(DcMotor.class, "armExtendMotor");
        clampOTron = hardwareMap.get(Servo.class, "clampOTron");
        backChompinator = hardwareMap.get(Servo.class, "chompinator1");
        frontChompinator = hardwareMap.get(Servo.class, "chompinator2");
        grabberPivoter = hardwareMap.get(CRServo.class, "grabberPivoter");

        drivetrain = new MecanumDrivetrain(new DcMotor[]{frontLeft, frontRight, backLeft, backRight});

        armLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        armLiftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armExtendMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        armLiftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        armLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);


    }

    public double getArmRaiserPower(float leftStickY, int armRaiseMotorPos) {

        double maxPowerRangeEnd = -1000;

        if (leftStickY < 0)
        {
            //Front to back
            if (armRaiseMotorPos < positionThreshold1) {
                //If inside the maximum power range
                return leftStickY * maxArmPowerMult; // no change
            }
            else if (armRaiseMotorPos < positionThreshold2 & armRaiseMotorPos >= positionThreshold1) {
                return leftStickY * midArmPowerMult;

            }
            else if (armRaiseMotorPos < positionThreshold3 & armRaiseMotorPos >= positionThreshold2) {
                return leftStickY * minArmPowerMult;
            }
            else {
                return 0.0;
            }
        }
        else {

                //back to front
                if (armRaiseMotorPos < positionThreshold1) {
                    //If inside the maximum power range
                    return leftStickY * minArmPowerMult; // no change
                }
                else if (armRaiseMotorPos < positionThreshold2 & armRaiseMotorPos >= positionThreshold1) {
                    return leftStickY * midArmPowerMult;

                }
                else if (armRaiseMotorPos < positionThreshold3 & armRaiseMotorPos >= positionThreshold2) {
                    return leftStickY * maxArmPowerMult;
                }
                else {
                    return 0.0;
                }


        }
    } // getMotorPower()


//java.lang.Math.cbrt()
    /**
     * User defined loop method
     * <p>
     * This method will be called repeatedly in a loop while this op mode is running
     */
    @Override
    public void loop() {


        //TODO Come back here and use a math function to set motor power on a curve
        armPos = armLiftMotor.getCurrentPosition();
        armExtendPower = gamepad2.right_stick_y * armExtendMult;
        // If arm is unlocked, set power

        if (armLockToggle == false) {
            armMotorPower = getArmRaiserPower(-gamepad2.left_stick_y, armPos);
        }
        if (armLockToggle == true) {
            if (armPos > 1200) {
                armMotorPower = -armLockPower;
            }

            if (armPos < 800) {
                armMotorPower = armLockPower;
            }
        }

        // Arm lock / unlock


        if (!gamepad2.x) {
            xNotPressed = true;
            telemetry.addData("Not pressed", "True");
        }

        if (xNotPressed == true && this.gamepad2.x) {
            xPressed = true;
            xNotPressed = false;
            telemetry.addData("Pressed","True");
        }

        if (xPressed == true) {
            armLockToggle = !armLockToggle;
            xPressed = false;
            telemetry.addData("State","Toggled");
        }

        // Command to control grabber rotation
        if (gamepad2.left_trigger > 0.1 && gamepad2.right_trigger > 0.1) {
            pivoterPower = 0.0;
        }
        else if (gamepad2.left_trigger > 0.1) {
            pivoterPower = gamepad2.left_trigger * pivoterMult;
            grabberPivoter.setDirection(DcMotorSimple.Direction.FORWARD);
        }
        else if (gamepad2.right_trigger > 0.1) {
            pivoterPower = gamepad2.right_trigger * pivoterMult;
            grabberPivoter.setDirection(DcMotorSimple.Direction.REVERSE);
        }
        else {
            pivoterPower = 0.0;
        }


        //Clamp-O-Tron controls (Block Clamp)
        if (this.gamepad2.a) {
            // Close
            clampOTron.setPosition(0.2);
        }

        if (this.gamepad2.b) {
           // open
            clampOTron.setPosition(0.8);
        }


        // Chompinator controls (Foundation Grippers)
        if (this.gamepad1.x) {
            // Open
            backChompinator.setPosition(0.22); // 0.3 -> straight up and down
            frontChompinator.setPosition(0.53); // 0.3 -> about 30 degrees off center
            //closer to one pulls the chompinators in
        }

        if (this.gamepad1.y) {
            // Close
            backChompinator.setPosition(0.585); // 0.6 -> one inch off the ground
            frontChompinator.setPosition(0.17); // 0.1 -> 3/4 inch off the ground
        }







        // Update drivetrain variables
        double course = Math.atan2(gamepad1.right_stick_y, gamepad1.right_stick_x) - Math.PI/2;
        double velocity = Math.hypot(gamepad1.right_stick_x, gamepad1.right_stick_y);
        double rotation = gamepad1.left_stick_x;

        // Update drivetrain commands
        drivetrain.setCourse(course);
        drivetrain.setVelocity(velocity);
        drivetrain.setRotation(rotation);

        // Update misc. motor powers
        armLiftMotor.setPower(armMotorPower); //Sets arm power
        armExtendMotor.setPower(armExtendPower);
        grabberPivoter.setPower(pivoterPower);

        // Telemetry
        telemetry.addData("course", course);
        telemetry.addData("velocity", velocity);
        telemetry.addData("rotation", rotation);
        telemetry.addData("armPos", armPos);
        telemetry.addData("armMotorPower", armMotorPower);
        telemetry.addData("lefty", gamepad2.left_stick_y);
        telemetry.addData("Arm Lcok", armLockToggle);
        telemetry.update();

    }
} // class mechanumRobotTeleop

