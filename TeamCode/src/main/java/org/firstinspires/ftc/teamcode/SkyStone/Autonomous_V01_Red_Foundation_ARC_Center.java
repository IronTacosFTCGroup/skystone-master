package org.firstinspires.ftc.teamcode.SkyStone;
// test 10/23

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.BNO055IMUImpl;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import edu.spa.ftclib.internal.controller.ErrorTimeThresholdFinishingAlgorithm;
import edu.spa.ftclib.internal.controller.FinishableIntegratedController;
import edu.spa.ftclib.internal.controller.PIDController;
import edu.spa.ftclib.internal.drivetrain.HeadingableMecanumDrivetrain;
import edu.spa.ftclib.internal.drivetrain.MecanumDrivetrain;
import edu.spa.ftclib.internal.sensor.IntegratingGyroscopeSensor;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
 */
@Autonomous
@Disabled





public class Autonomous_V01_Red_Foundation_ARC_Center extends LinearOpMode {


    // Creates Phases
    public enum Phase {
        START, STRAFE_RIGHT1, REVERSE1, DRIVE_FORWARD1, STRAFE_LEFT1, REVERSE2,STRAFE_RIGHT2, STRAFE_LEFT2, DRIVE_FORWARD2, STRAFE_RIGHT3, DRIVE_FORWARD3, ALIGN, PARK, DONE;
    }

    // Variables
    public boolean armLockToggle = false;
    public double armMotorPower = 0.0;
    public int armPos = 0;

    // Constants
    public double ARM_LOCK_POWER = 0.00071; // Power to lock the arm in one place

    // Actuators
    public DcMotor frontLeft;
    public DcMotor frontRight;
    public DcMotor backLeft;
    public DcMotor backRight;

    public DcMotor armLiftMotor;        //motor that lifts arm
    public Servo clampOTron;            //hand that grabs the blocks
    public Servo backChompinator;          //base grabbers
    public Servo frontChompinator;
    public Servo blockClamp;
    public CRServo grabberPivoter;      // Spins the clampotron

    public MecanumDrivetrain drivetrain;

    public FinishableIntegratedController controller;

   // public BNO055IMUImpl imu;

    // Driving function

    public void Drive(double speed, double course, double time) {

        sleep(250);

        drivetrain.setCourse(Math.toRadians(course));
        drivetrain.setVelocity(speed / 100);

        sleep((long)(time * 1000)); // Waits for set time

        drivetrain.setVelocity(0.0); // Stops the drivtrain after set time
    }

    // Clamp function

    public void Chomp(boolean clamped) {

        if (clamped == true) {
            frontChompinator.setPosition(0.22);
            backChompinator.setPosition(0.53);
            sleep(500);
        }

        if (clamped == false) {
            frontChompinator.setPosition(0.53);
            backChompinator.setPosition(0.22);
            sleep(200);

        }
    }

    public void Clamp(boolean clamped) {

        if (clamped == true) {

            blockClamp.setPosition(0.9);

            sleep(1000);
        }

        if (clamped == false) {


            blockClamp.setPosition(0.5);

            sleep(1000);

        }
    }



    @Override
    public void runOpMode() {
        // Properties
        // Gets hardware info and names each actuator
        frontLeft = hardwareMap.get(DcMotor.class, "driveFrontLeft");
        frontRight = hardwareMap.get(DcMotor.class, "driveFrontRight");
        backLeft = hardwareMap.get(DcMotor.class, "driveBackLeft");
        backRight = hardwareMap.get(DcMotor.class, "driveBackRight");

        armLiftMotor = hardwareMap.get(DcMotor.class, "armLiftMotor");
        clampOTron = hardwareMap.get(Servo.class, "clampOTron");
        backChompinator = hardwareMap.get(Servo.class, "chompinator1");
        frontChompinator = hardwareMap.get(Servo.class, "chompinator2");
        grabberPivoter = hardwareMap.get(CRServo.class, "grabberPivoter");
        blockClamp = hardwareMap.get(Servo.class, "blockClamp");




        //Controller / PID Setup

/*
        imu = hardwareMap.get(BNO055IMUImpl.class, "imu");
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit = BNO055IMU.AngleUnit.RADIANS;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        //Add calibration file?
        parameters.loggingEnabled = true;   //For debugging
        parameters.loggingTag = "IMU";      //For debugging
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();  //Figure out why the naive one doesn't have a public constructor
        imu.initialize(parameters);
        while (!imu.isGyroCalibrated());


        PIDController pid = new PIDController(1.5, 0.05, 0);
        pid.setMaxErrorForIntegral(0.002);

        controller = new FinishableIntegratedController(new IntegratingGyroscopeSensor(imu), pid, new ErrorTimeThresholdFinishingAlgorithm(Math.PI/50, 1));
  */      drivetrain = new HeadingableMecanumDrivetrain(new DcMotor[]{frontLeft,frontRight, backLeft, backRight}, controller);
        for (DcMotor motor : drivetrain.motors) motor.setDirection(DcMotor.Direction.REVERSE);  //Depending on the design of the robot, you may need to comment this line out.
        // for (DcMotor motor : drivetrain.motors) motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // Sets phase
        Phase currentPhase = Phase.START;




        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {



            // Arm lock / unlock

            telemetry.addData("Phase", currentPhase);
            telemetry.update();

            armPos = armLiftMotor.getCurrentPosition();


            switch (currentPhase)
            {
                case START:

                    currentPhase = Phase.STRAFE_RIGHT1;
                    break;

                case STRAFE_RIGHT1:

                    armLiftMotor.setPower(ARM_LOCK_POWER);

                    Drive(50,90,2.15);

                    currentPhase = Phase.REVERSE1;
                    break;

                case REVERSE1:

                    Drive(40, 180, 0.8);

                    currentPhase = Phase.DRIVE_FORWARD1;
                    break;

                case DRIVE_FORWARD1:

                    Drive(20, 0, 0.15);

                    currentPhase = Phase.STRAFE_LEFT1;
                    break;

                case STRAFE_LEFT1:

                    Drive(30, -90, 2.1);


                    currentPhase = Phase.REVERSE2;
                    break;

                case REVERSE2:

                    Drive(40,180, 1.55);





                    currentPhase = Phase.DRIVE_FORWARD2;
                    break;

                case DRIVE_FORWARD2:

                    sleep(500);

                    Drive(30,0,0.2);


                    currentPhase = Phase.STRAFE_RIGHT2;
                    break;

                case STRAFE_RIGHT2:

                    Drive(30,90,0.95);
                    Chomp(true);

                    currentPhase = Phase.STRAFE_LEFT2;
                    break;



                case STRAFE_LEFT2:

                    Drive(70,-90, 4);



                    currentPhase = Phase.DRIVE_FORWARD3;
                    break;

                case DRIVE_FORWARD3:

                    Drive(30,0,0.3);

                    currentPhase = Phase.STRAFE_RIGHT3;
                    break;

                case STRAFE_RIGHT3:

                    Drive(50,90,2.5);
                    Chomp(false);

                    currentPhase = Phase.ALIGN;
                    break;


                case ALIGN:

                    Drive(20,-90,0.2);

                    Drive(40,180,1);

                    Drive(40,0,1);

                    currentPhase = Phase.PARK;
                    break;


                case PARK:

                    Drive(50,-90, 1.8);

                    currentPhase = Phase.DONE;
                    break;

                case DONE:
                    sleep(3000);
                    Clamp(true);


                    break;


            }// End switching phases
        } // while op mode active

    } // runOpMode()
} // class declaration

