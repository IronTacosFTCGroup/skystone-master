package org.firstinspires.ftc.teamcode.UltimateGoal;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 *
 Use this op mode to test the launcher.
 To set up plug the launcher motor into the 0 slot on the drive hub.
 Use controller 1
 A = Full speed
 B = Stop
 X = Full speed
 Y = 75% speed


 **/

//@Disabled
//@TeleOp(name = "Mecanum Robot Tele-op", group = "sample")
@TeleOp
public class IntakeTest extends OpMode {





    public DcMotor intake1;
    public DcMotor intake2;






    public double power = 0.0;



    /**
     * User defined init method
     * <p>
     * This method will be called once when the INIT button is pressed.
     */
    @Override
    public void init() {



        intake1 = hardwareMap.get(DcMotor.class, "intake1");
        intake2 = hardwareMap.get(DcMotor.class, "intake2");


 /*       armLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        armLiftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armExtendMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        armLiftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        armLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER); */


    }




    public void launcherContorl() {
        // Update drivetrain variables


        if (gamepad1.a) {
            power = 1.0;

        }
        if (gamepad1.b) {
            power = 0.0;

        }

        if (gamepad1.x) {
            power = -1;

        }


        intake1.setPower(1 * power);
        intake2.setPower(power);




    }

    public void runTelemetry() {
        // Telemetry



        telemetry.addData("Power", power);
        telemetry.update();
    }

    public void runTeleOpCommands() {



        launcherContorl();

        runTelemetry();


    }



    /**
     * User defined loop method
     * <p>
     * This method will be called repeatedly in a loop while this op mode is running
     */
    @Override
    public void loop() {

        runTeleOpCommands();

    }
}

