package org.firstinspires.ftc.teamcode.RoverRukus;
// test 10/23

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;


//Mineral Detection

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
 */
@Autonomous
@Disabled

public class AutonomousV_0_7_Crater extends LinearOpMode {

    // The IMU sensor object
    BNO055IMU imu;

    // State used for updating telemetry
    Orientation angles;
    Acceleration gravity;

    //Actuators
    private DcMotor leftDriveMotor; // Left drive motor
    private DcMotor rightDriveMotor; // Right drive motor
    private DcMotor robotLiftMotor; // lifting motor

    private Servo hookServo; // hook servo

    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";
    private static final String VUFORIA_KEY = "AVya6t7/////AAABmTZybxAXJk+alcs417OOIRRBuvbJJJhz2do6X0fVrc7EBp+uaTnt3CtVJ1gYeSCRga45fI5Z+MOAOPQZJGH9xHZ4vl/0pi7baRH8VLygHowUZSmBIt9dWAhLPw2dXQx7ZcLgnyZo2gVfu1T5kJpfGVf6D/3ueuClxI+NsnqQLH8WAmFX7hvVElkbxFslx1XHv2xoQYys9URLfidSFh5No9yG8U2M+T2DU5h3QNF/tVBdFS/Mw/HVSh5gsxHQBba2WzIA0w5SH5T4y+ypYQfIJsgX7sCX8xrW/6uZc9qBREguqX4oCy8AL3YZugY1FIx4PcGL+F+7OBATPDug3PO2HEr81YJcLuk1bM+i2zKP4bAs";

    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;


    @Override
    public void runOpMode() {
        // Properties
        // Gets hardware info and names each actuator
        leftDriveMotor = hardwareMap.get(DcMotor.class, "leftDriveMotor"); // Left drive motor
        rightDriveMotor = hardwareMap.get(DcMotor.class, "rightDriveMotor"); // Right drive motor
        robotLiftMotor = hardwareMap.get(DcMotor.class, "robotLiftMotor"); // robot lifting motor

        hookServo = hardwareMap.get(Servo.class, "hookServo"); // hook servo


        // Creates Phases
        final int DETECT_MINERALS = 1;
        final int LOWER_ROBOT  = 2;
        final int DRIVE_FROM_LANDER = 3;
        final int TURN = 4;
        final int ARC = 5;
        final int IDLE = 7;

        // Sets phase
        int currentPhase = DETECT_MINERALS;

        //Names all variables, sets values
        double linActLowerDistance = -26550; // used to change the distance we lower the robot
        double straightDriveDistance = -1610; // driving away from the lander
        double turnDistance = 275;
        double arcDistance = 2500;
        double accThreshold = 100000;

        double MINERAL_CENTER_DISTANCE = -1610;
        double MINERAL_LR_DISTANCE = -200;

        int MINERAL_LEFT = 0;
        int MINERAL_CENTER = 1;
        int MINERAL_RIGHT = 2;
        int mineralPosition = MINERAL_CENTER;
        int cameracounter = 0;

        int ACC_REST = 0;
        int ACC_SPEEDING = 1;
        int ACC_MOVING = 2;
        int ACC_SLOWING = 3;
        int accStatus = ACC_REST;



        double WHEEL_POWER_MLUT = 0.3; // the power to the wheels
        double LOWER_ROBOT_POWER_MLUT = 4.0; // Controls how fast linear actuator extends
        double INSIDE_WHEEL_MULT = 0.5;

        int leftDrivePos = 0; // Used to measure the left drive motor's position
        int rightDrivePos = 0;
        int robotLiftPos = 0; // Used to measure the linear actuator motor's position;

        Acceleration acc;
        double accScalar;

        // Configures motors to use encoders and brake when no power is applied
        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        leftDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robotLiftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // Relays status to driver station.
        telemetry.addData("Status", "Initialized");
        telemetry.update();


        initVuforia();

        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        } else {
            telemetry.addData("Sorry!", "This device is not compatible with TFOD");
        }



        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        /** Activate Tensor Flow Object Detection. */
        if (tfod != null) {
            tfod.activate();
            telemetry.addData("activating tfod", "");
            sleep(1000);
        }


        // Run until the end of the match (driver presses STOP)/
        while (opModeIsActive()) {
            // Update Variables
            leftDrivePos = leftDriveMotor.getCurrentPosition();
            rightDrivePos = rightDriveMotor.getCurrentPosition();
            robotLiftPos = robotLiftMotor.getCurrentPosition();
            acc = imu.getAcceleration();
            accScalar = Math.sqrt(acc.xAccel * acc.xAccel + acc.yAccel * acc.yAccel + acc.zAccel * acc.zAccel);

            telemetry.addData("WheelP", leftDrivePos);
            telemetry.addData("Lin Act Pos", robotLiftPos);

            accThreshold = leftDrivePos < 200 ? 100000 : 1000;

            switch (currentPhase)
            {
                case DETECT_MINERALS:
                    cameracounter++;

                    if (cameracounter > 1000000) {
                        currentPhase = LOWER_ROBOT;
                    }


                    if (tfod != null) {
                        // getUpdatedRecognitions() will return null if no new information is available since
                        // the last time that call was made.
                        List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                        if (updatedRecognitions != null) {
                            telemetry.addData("# Object Detected", updatedRecognitions.size());
                            if (updatedRecognitions.size() == 3) {
                                int goldMineralX = -1;
                                int silverMineral1X = -1;
                                int silverMineral2X = -1;
                                for (Recognition recognition : updatedRecognitions) {
                                    if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                                        goldMineralX = (int) recognition.getLeft();
                                    } else if (silverMineral1X == -1) {
                                        silverMineral1X = (int) recognition.getLeft();
                                    } else {
                                        silverMineral2X = (int) recognition.getLeft();
                                    }
                                }
                                if (goldMineralX != -1 && silverMineral1X != -1 && silverMineral2X != -1) {
                                    if (goldMineralX < silverMineral1X && goldMineralX < silverMineral2X) {
                                        telemetry.addData("Gold Mineral Position", "Left");
                                        mineralPosition = MINERAL_LEFT;
                                    } else if (goldMineralX > silverMineral1X && goldMineralX > silverMineral2X) {
                                        telemetry.addData("Gold Mineral Position", "Right");
                                        mineralPosition = MINERAL_RIGHT;
                                    } else {
                                        telemetry.addData("Gold Mineral Position", "Center");
                                        mineralPosition = MINERAL_CENTER;

                                    }
                                    currentPhase = LOWER_ROBOT;

                                }
                            }
                            telemetry.update();
                        } else {
                            telemetry.addData("object NOT detected", "");
                        }
                    }

                break;

                case LOWER_ROBOT:

                    telemetry.addData("Status", "Lowering");
                    telemetry.addData("Accel X", acc.xAccel);
                    telemetry.addData("Accel Y", acc.yAccel);
                    telemetry.addData("Accel Z", acc.zAccel);
                    telemetry.addData("Scalar", accScalar);

                    if (robotLiftPos  > linActLowerDistance) {
                        robotLiftMotor.setPower(-1 * LOWER_ROBOT_POWER_MLUT);
                        telemetry.addData("Distance Lowered", robotLiftPos);
                    }
                    else
                    {
                        telemetry.addData("Failsafe", "Activated");
                        robotLiftMotor.setPower(0.0);
                        hookServo.setPosition(0.85);
                        sleep(1000);
                        currentPhase = DRIVE_FROM_LANDER;
                    }

                    if (accStatus == ACC_REST) {
                        if (acc.zAccel > 0.1) {
                            accStatus = ACC_SPEEDING;
                        }
                    }
                    if (accStatus == ACC_SPEEDING) {
                        if (acc.zAccel < 0.1) {
                            accStatus = ACC_MOVING;
                        }
                    }
                    if (accStatus == ACC_MOVING) {
                        if (acc.zAccel < -0.1) {
                            accStatus = ACC_SLOWING;
                        }
                    }
                    if (accStatus == ACC_SLOWING) {
                        telemetry.addData("Imu", "Used");
                        sleep(250);
                        robotLiftMotor.setPower(0.0);
                        hookServo.setPosition(0.85);
                        currentPhase = DRIVE_FROM_LANDER;
                    }


                break;

                case DRIVE_FROM_LANDER:

                    telemetry.addData("Status", "Driving Depot");

                    robotLiftMotor.setPower(0.7);
                    if (mineralPosition == MINERAL_CENTER) {
                        straightDriveDistance = MINERAL_CENTER_DISTANCE;
                    }
                    else {
                        straightDriveDistance = MINERAL_LR_DISTANCE;
                    }


                    if (leftDrivePos > straightDriveDistance && accScalar < accThreshold) {
                        leftDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                        rightDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                    }
                    else {
                        leftDriveMotor.setPower(0.0);
                        rightDriveMotor.setPower(0.0);

                        if (mineralPosition == MINERAL_CENTER) {
                            currentPhase = IDLE;
                            resetEncoders();
                        }
                        else {
                            currentPhase = TURN;
                            resetEncoders();
                        }

                    }

                    break;

                case TURN:

                    if (mineralPosition == MINERAL_LEFT) {
                        if (leftDrivePos < turnDistance) {
                            leftDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = ARC;
                            resetEncoders();
                        }
                    }

                    if (mineralPosition == MINERAL_RIGHT) {
                        if (leftDrivePos > -turnDistance) {
                            leftDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = ARC;
                            resetEncoders();
                        }
                    }

                break;

                case ARC:

                    if (mineralPosition == MINERAL_LEFT) {
                        if (leftDrivePos > -arcDistance && accScalar < accThreshold) {
                            leftDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(1 * WHEEL_POWER_MLUT * INSIDE_WHEEL_MULT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = IDLE;
                            resetEncoders();
                        }
                    }

                    if (mineralPosition == MINERAL_RIGHT) {
                        if (rightDrivePos < arcDistance && accScalar < accThreshold) {
                            leftDriveMotor.setPower(-1 * WHEEL_POWER_MLUT * INSIDE_WHEEL_MULT);
                            rightDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = IDLE;
                            resetEncoders();
                        }
                    }

                break;


                case IDLE:
                    sleep(3000);
                    robotLiftMotor.setPower(0.0);
                    // In case we want to do anything while stopped

                default: // something is wrong, stop motors


                        robotLiftMotor.setPower(0.0);
                        leftDriveMotor.setPower(0.0);
                        rightDriveMotor.setPower(0.0);
                        telemetry.addData("F", "in the chat");


                break;

            }// End switching phases
            telemetry.update();
        } // while op mode active
    } // runOpMode()

    private void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraName = hardwareMap.get(WebcamName.class, "Webcam 1");

        //  Instantiate the Vuforia engine
        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        // Loading trackables is not necessary for the Tensor Flow Object Detection engine.
    }

    /**
     * Initialize the Tensor Flow Object Detection engine.
     */
    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }
    private void resetEncoders(){

        leftDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

    }
}
