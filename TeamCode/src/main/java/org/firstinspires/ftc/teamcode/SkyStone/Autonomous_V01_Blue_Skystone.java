//package org.firstinspires.ftc.teamcode.SkyStone;
//// test 10/23
//
//import com.qualcomm.hardware.bosch.BNO055IMU;
//import com.qualcomm.hardware.bosch.BNO055IMUImpl;
//import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
//import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
//import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
//import com.qualcomm.robotcore.hardware.CRServo;
//import com.qualcomm.robotcore.hardware.DcMotor;
//import com.qualcomm.robotcore.hardware.Servo;
//
//import org.firstinspires.ftc.robotcore.external.ClassFactory;
//import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
//import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
//import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
//import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
//
//import java.util.List;
//
//import edu.spa.ftclib.internal.controller.ErrorTimeThresholdFinishingAlgorithm;
//import edu.spa.ftclib.internal.controller.FinishableIntegratedController;
//import edu.spa.ftclib.internal.controller.PIDController;
//import edu.spa.ftclib.internal.drivetrain.HeadingableMecanumDrivetrain;
//import edu.spa.ftclib.internal.drivetrain.MecanumDrivetrain;
//import edu.spa.ftclib.internal.sensor.IntegratingGyroscopeSensor;
//
///**
// * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
// * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
// * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
// * class is instantiated on the Robot Controller and executed.
// *
// * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
// * It includes all the skeletal structure that all linear OpModes contain.
// *
// * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
// * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
// */
//@Autonomous
////@Disabled
//
//
//
//
//
//
//public class Autonomous_V01_Blue_Skystone extends LinearOpMode {
//
//    // Vuforia
//
//    private static final String TFOD_MODEL_ASSET = "Skystone.tflite";
//    private static final String LABEL_FIRST_ELEMENT = "Stone";
//    private static final String LABEL_SECOND_ELEMENT = "Skystone";
//
//    /*
//     * IMPORTANT: You need to obtain your own license key to use Vuforia. The string below with which
//     * 'parameters.vuforiaLicenseKey' is initialized is for illustration only, and will not function.
//     * A Vuforia 'Development' license key, can be obtained free of charge from the Vuforia developer
//     * web site at https://developer.vuforia.com/license-manager.
//     *
//     * Vuforia license keys are always 380 characters long, and look as if they contain mostly
//     * random data. As an example, here is a example of a fragment of a valid key:
//     *      ... yIgIzTqZ4mWjk9wd3cZO9T1axEqzuhxoGlfOOI2dRzKS4T0hQ8kT ...
//     * Once you've obtained a license key, copy the string from the Vuforia web site
//     * and paste it in to your code on the next line, between the double quotes.
//     */
//    private static final String VUFORIA_KEY =
//            "AVya6t7/////AAABmTZybxAXJk+alcs417OOIRRBuvbJJJhz2do6X0fVrc7EBp+uaTnt3CtVJ1gYeSCRga45fI5Z+MOAOPQZJGH9xHZ4vl/0pi7baRH8VLygHowUZSmBIt9dWAhLPw2dXQx7ZcLgnyZo2gVfu1T5kJpfGVf6D/3ueuClxI+NsnqQLH8WAmFX7hvVElkbxFslx1XHv2xoQYys9URLfidSFh5No9yG8U2M+T2DU5h3QNF/tVBdFS/Mw/HVSh5gsxHQBba2WzIA0w5SH5T4y+ypYQfIJsgX7sCX8xrW/6uZc9qBREguqX4oCy8AL3YZugY1FIx4PcGL+F+7OBATPDug3PO2HEr81YJcLuk1bM+i2zKP4bAs";
//
//    /**
//     * {@link #vuforia} is the variable we will use to store our instance of the Vuforia
//     * localization engine.
//     */
//    private VuforiaLocalizer vuforia;
//
//    /**
//     * {@link #tfod} is the variable we will use to store our instance of the TensorFlow Object
//     * Detection engine.
//     */
//    private TFObjectDetector tfod;
//
//
//
//    // Creates Phases
//    public enum Phase {
//        START, STRAFE_LEFT1, SCAN1, STRAFE_RIGHT1, REVERSE1, DRIVE_FORWARD1, STRAFE_LEFT2, SCAN2, STRAFE_RIGHT2, REVERSE2, DRIVE_FORWARD2, DONE;
//    }
//
//    // Variables
//    public boolean armLockToggle = false;
//    public double armMotorPower = 0.0;
//    public int armPos = 0;
//
//    public int skystone1Position = 0;
//    public int skystone2Position = 0;
//
//    public int [] skystonePositions = {0,0,0,0,0,0};
//    public int skystonesDetected = 0;
//
//        // Stone position toggles (0: Regular Stone, 1: Skystone)
//    public int stone1 = 0;
//    public int stone2 = 0;
//    public int stone3 = 0;
//    public int stone4 = 0;
//    public int stone5 = 0;
//    public int stone6 = 0;
//
//    // Constants
//    public double ARM_LOCK_POWER = 0.00071; // Power to lock the arm in one place
//
//    private static final String LABEL_SKYSTONE = "SKYSTONE";
//
//    // Actuators
//    public DcMotor frontLeft;
//    public DcMotor frontRight;
//    public DcMotor backLeft;
//    public DcMotor backRight;
//
//    public DcMotor armLiftMotor;        //motor that lifts arm
//    public Servo clampOTron;            //hand that grabs the blocks
//    public Servo backChompinator;          //base grabbers
//    public Servo frontChompinator;
//    public Servo blockClamp;
//    public CRServo grabberPivoter;      // Spins the clampotron
//
//    public MecanumDrivetrain drivetrain;
//
//    public FinishableIntegratedController controller;
//
//    public BNO055IMUImpl imu;
//
//    // Driving function
//
//    public void Drive(double speed, double course, double time) {
//
//        sleep(250);
//
//        drivetrain.setCourse(Math.toRadians(course));
//        drivetrain.setVelocity(speed / 100);
//
//        sleep((long)(time * 1000)); // Waits for set time
//
//        drivetrain.setVelocity(0.0); // Stops the drivtrain after set time
//    }
//
//    // Clamp function
//
//    public void Chomp(boolean clamped) {
//
//        if (clamped == true) {
//            frontChompinator.setPosition(0.22);
//            backChompinator.setPosition(0.53);
//            sleep(1000);
//        }
//
//        if (clamped == false) {
//            frontChompinator.setPosition(0.53);
//            backChompinator.setPosition(0.22);
//            sleep(1000);
//
//        }
//    }
//
//    public void Clamp(boolean clamped) {
//
//        if (clamped == true) {
//
//            blockClamp.setPosition(0.9);
//
//           // sleep(1000);
//        }
//
//        if (clamped == false) {
//
//            blockClamp.setPosition(0.5);
//
//           // sleep(1000);
//
//        }
//    }
//
//    private void scan(){
//
//        if (tfod != null)
//        {
//            // getUpdatedRecognitions() will return null if no new information is available since
//            // the last time that call was made.
//            List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
//            if (updatedRecognitions != null)
//            {
//
//                for (Recognition recognition : updatedRecognitions)
//                {
//
//                    if (recognition.getLabel().toUpperCase().equals(LABEL_SKYSTONE)) {
//                        telemetry.addData("skystone", "detected");
//                        telemetry.update();
//
//                        if (recognition.getLeft() > bruh && recognition.getRight() > bruh) {
//
//                            // Right
//
//                             skystone1Position = POSITION_RIGHT;
//
//
//                        }
//
//                        if (recognition.getLeft() > bruh && recognition.getRight() > bruh) {
//
//                            // Center
//
//                            skystone1Position = POSITION_CENTER;
//
//
//
//
//
//                    }  // If detected object is a skystone
//
//
//
//                    else {
//
//                        // Regular Stones
//
//
//                        regularStones++;
//                        if (regularStones > 1) {
//                          skystone1Position = POSITION_LEFT;
//                        }
//
//
//
//
//                    }
//
//
//
//                } // FOR each stone detected loop
//
//            } // Update Scan
//
//        } // If TFOD is not broken
//
//    } // Scan Function
//
//
//    /**
//     * Initialize the Vuforia localization engine.
//     */
//    private void initVuforia() {
//        /*
//         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
//         */
//        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();
//
//        parameters.vuforiaLicenseKey = VUFORIA_KEY;
//        parameters.cameraName = hardwareMap.get(WebcamName.class, "Webcam 1");
//
//        //  Instantiate the Vuforia engine
//        vuforia = ClassFactory.getInstance().createVuforia(parameters);
//
//        // Loading trackables is not necessary for the TensorFlow Object Detection engine.
//    }
//
//    /**
//     * Initialize the TensorFlow Object Detection engine.
//     */
//    private void initTfod() {
//        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
//                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
//        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
//        tfodParameters.minimumConfidence = 0.8;
//        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
//        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_FIRST_ELEMENT, LABEL_SECOND_ELEMENT);
//    }
//
//
//
//
//
//    @Override
//    public void runOpMode() {
//        // Properties
//        // Gets hardware info and names each actuator
//        frontLeft = hardwareMap.get(DcMotor.class, "driveFrontLeft");
//        frontRight = hardwareMap.get(DcMotor.class, "driveFrontRight");
//        backLeft = hardwareMap.get(DcMotor.class, "driveBackLeft");
//        backRight = hardwareMap.get(DcMotor.class, "driveBackRight");
//
//        armLiftMotor = hardwareMap.get(DcMotor.class, "armLiftMotor");
//        clampOTron = hardwareMap.get(Servo.class, "clampOTron");
//        backChompinator = hardwareMap.get(Servo.class, "chompinator1");
//        frontChompinator = hardwareMap.get(Servo.class, "chompinator2");
//        grabberPivoter = hardwareMap.get(CRServo.class, "grabberPivoter");
//        blockClamp = hardwareMap.get(Servo.class,"blockClamp");
//
//
//
//
//        //Controller / PID Setup
//
//
//        imu = hardwareMap.get(BNO055IMUImpl.class, "imu");
//        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
//        parameters.angleUnit = BNO055IMU.AngleUnit.RADIANS;
//        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
//        //Add calibration file?
//        parameters.loggingEnabled = true;   //For debugging
//        parameters.loggingTag = "IMU";      //For debugging
//        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();  //Figure out why the naive one doesn't have a public constructor
//        imu.initialize(parameters);
//        while (!imu.isGyroCalibrated());
//
//
//        PIDController pid = new PIDController(1.5, 0.05, 0);
//        pid.setMaxErrorForIntegral(0.002);
//
//        controller = new FinishableIntegratedController(new IntegratingGyroscopeSensor(imu), pid, new ErrorTimeThresholdFinishingAlgorithm(Math.PI/50, 1));
//        drivetrain = new HeadingableMecanumDrivetrain(new DcMotor[]{frontLeft,frontRight, backLeft, backRight}, controller);
//        for (DcMotor motor : drivetrain.motors) motor.setDirection(DcMotor.Direction.REVERSE);  //Depending on the design of the robot, you may need to comment this line out.
//        // for (DcMotor motor : drivetrain.motors) motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
//
//        // Sets phase
//        Phase currentPhase = Phase.START;
//
//        // The TFObjectDetector uses the camera frames from the VuforiaLocalizer, so we create that
//        // first.
//        initVuforia();
//
//        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
//            initTfod();
//        } else {
//            telemetry.addData("Sorry!", "This device is not compatible with TFOD");
//        }
//
//        /**
//         * Activate TensorFlow Object Detection before we wait for the start command.
//         * Do it here so that the Camera Stream window will have the TensorFlow annotations visible.
//         **/
//        if (tfod != null) {
//            tfod.activate();
//        }
//
//
//
//
//        // Wait for the game to start (driver presses PLAY)
//        waitForStart();
//
//        // Run until the end of the match (driver presses STOP)
//        while (opModeIsActive()) {
//
//
//            // Vuforia Deactivate
//
//                if (tfod != null) {
//                    tfod.shutdown();
//                }
//
//
//
//            // Arm lock / unlock
//
//            telemetry.addData("Phase", currentPhase);
//            telemetry.update();
//
//            armPos = armLiftMotor.getCurrentPosition();
//
//
//            switch (currentPhase)
//            {
//                case START:
//
//                    currentPhase = Phase.STRAFE_LEFT1
//
//                    break;
//
//                case STRAFE_LEFT1:
//
//                    Drive(20,-90,0.3);
//
//                    currentPhase = Phase.SCAN;
//                    break;
//
//                case SCAN:
//
//                    scan();
//
//                    tfod.deactivate();
//
//                    if (skystone1position > 0) {
//                        currentPhase = Phase.DRIVE_FORWARD1;
//                    }
//
//
//                    break;
//
//                case DRIVE_FORWARD1:
//
//                    Drive(20,0,);
//
//                    currentPhase = Phase.STRAFE_RIGHT1;
//                    }
//
//                    break;
//
//                case STRAFE_LEFT2:
//
//                    Drive(0, 0, 0);

//                    clamp(true);
//
//                    currentPhase = Phase.STRAFE_RIGHT1;
//                    break;
//
//                case STRAFE_RIGHT1:
//
//                    Drive(0, 0, 0);
//
//
//                    currentPhase = Phase.REVERSE;
//                    break;
//
//                case REVERSE:
//
//                    Drive(0,0, 0);
//                    clamp(false);
//
//
//
//                    currentPhase = Phase.DRIVE_FORWARD2;
//                    break;
//
//                case DRIVE_FORWARD2:
//
//                    Drive(0,0,0);
//
//
//                    currentPhase = Phase.DONE;
//                    break;
//
//
//
//
//                case DONE:
//
//
//
//                    break;
//
//
//            }// End switching phases
//        } // while op mode active
//
//    } // runOpMode()
//} // class declaration
//
